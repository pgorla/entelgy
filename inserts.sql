INSERT INTO `bread` VALUES (1,'Italiano',1.00),(2,'Francês',2.00);
INSERT INTO `cheese` VALUES (1,'Prato',1.00),(2,'Chedar',0.50),(3,NULL,NULL);
INSERT INTO `filling` VALUES (1,'Carne',0.50),(2,'Frango',1.00);
INSERT INTO `salad` VALUES (1,'Acelga',0.50),(2,'Alface',1.00);
INSERT INTO `sauce` VALUES (1,'Especial',1.50),(2,'Picante',2.00);
INSERT INTO `spice` VALUES (1,'Pimenta',1.50),(2,'Orégano',0.50),(3,'Cominho',1.00);

INSERT INTO `snacks` VALUES (1,1,1,1,1,1,1),(2,2,1,2,2,1,2),(3,1,2,2,1,1,1);
