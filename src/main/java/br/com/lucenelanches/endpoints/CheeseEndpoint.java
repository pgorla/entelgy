package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.resource.CheeseResource;
import br.com.lucenelanches.service.CheeseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CheeseEndpoint {

    @Autowired
    private CheeseService cheeseService;

    @RequestMapping("/cheese")
    List<CheeseResource> home() {
        return cheeseService.getAll();
    }


}
