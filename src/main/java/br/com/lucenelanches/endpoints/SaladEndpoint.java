package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.resource.SaladResource;
import br.com.lucenelanches.service.SaladService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SaladEndpoint {

    @Autowired
    private SaladService saladService;

    @RequestMapping("/salad")
    List<SaladResource> home() {

        return saladService.getAll();
    }


}
