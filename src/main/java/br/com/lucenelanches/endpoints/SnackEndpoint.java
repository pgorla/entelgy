package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.resource.SnackResource;
import br.com.lucenelanches.service.SnackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SnackEndpoint {

    @Autowired
    private SnackService snackService;

    @RequestMapping("/snack")
    Iterable<SnackResource> home() {

        return snackService.getAll();
    }


}
