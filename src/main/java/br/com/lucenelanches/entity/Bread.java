package br.com.lucenelanches.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "bread")
public class Bread {

    @Id
    @Column(name = "idt_bread")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "bread")
    String bread;

    @Column(name = "value")
    BigDecimal value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBread() {
        return bread;
    }

    public void setBread(String bread) {
        this.bread = bread;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
