package br.com.lucenelanches.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "cheese")
public class Cheese {

    @Id
    @Column(name = "idt_cheese")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "cheese")
    String cheese;

    @Column(name = "value")
    BigDecimal value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCheese() {
        return cheese;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
