package br.com.lucenelanches.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "filling")
public class Filling {

    @Id
    @Column(name = "idt_filling")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "filling")
    String filling;

    @Column(name = "value")
    BigDecimal value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilling() {
        return filling;
    }

    public void setFilling(String filling) {
        this.filling = filling;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
