package br.com.lucenelanches.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "spice")
@Table(name = "spice")
public class Spice {

    @Id
    @Column(name = "idt_spice")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "spice")
    String spice;

    @Column(name = "value")
    BigDecimal value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpice() {
        return spice;
    }

    public void setSpice(String spice) {
        this.spice = spice;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
