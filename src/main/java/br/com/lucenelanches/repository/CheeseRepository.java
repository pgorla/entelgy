package br.com.lucenelanches.repository;


import br.com.lucenelanches.entity.Cheese;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository(value = "cheeseRepository")
@Component
public interface CheeseRepository extends JpaRepository<Cheese, Long> {

}
