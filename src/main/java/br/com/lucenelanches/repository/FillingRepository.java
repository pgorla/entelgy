package br.com.lucenelanches.repository;


import br.com.lucenelanches.entity.Filling;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository(value = "fillingRepository")
@Component
public interface FillingRepository extends JpaRepository<Filling, Long> {

}
