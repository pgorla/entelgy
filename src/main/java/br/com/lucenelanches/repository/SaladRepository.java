package br.com.lucenelanches.repository;


import br.com.lucenelanches.entity.Salad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository(value = "saladRepository")
@Component
public interface SaladRepository extends JpaRepository<Salad, Long> {

}
