package br.com.lucenelanches.resource;

public class SnackResource {

    Long id;

    BreadResource bread;

    CheeseResource cheese;

    FillingResource filling;

    SauceResource sauce;

    SpiceResource spice;

    SaladResource salad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BreadResource getBread() {
        return bread;
    }

    public void setBread(BreadResource bread) {
        this.bread = bread;
    }

    public CheeseResource getCheese() {
        return cheese;
    }

    public void setCheese(CheeseResource cheese) {
        this.cheese = cheese;
    }

    public FillingResource getFilling() {
        return filling;
    }

    public void setFilling(FillingResource filling) {
        this.filling = filling;
    }

    public SauceResource getSauce() {
        return sauce;
    }

    public void setSauce(SauceResource sauce) {
        this.sauce = sauce;
    }

    public SpiceResource getSpice() {
        return spice;
    }

    public void setSpice(SpiceResource spice) {
        this.spice = spice;
    }

    public SaladResource getSalad() {
        return salad;
    }

    public void setSalad(SaladResource salad) {
        this.salad = salad;
    }
}
