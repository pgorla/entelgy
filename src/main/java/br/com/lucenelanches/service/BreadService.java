package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Bread;
import br.com.lucenelanches.repository.BreadRepository;
import br.com.lucenelanches.resource.BreadResource;
import br.com.lucenelanches.util.BreadConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "breadService")
@Component
public class BreadService {

    @Autowired
    BreadRepository breadRepository;

    public List<BreadResource> getAll() {

        List<Bread> breads = breadRepository.findAll();

        List<BreadResource> breadResources = BreadConverter.convertList(breads);

        return breadResources;

    }
}
