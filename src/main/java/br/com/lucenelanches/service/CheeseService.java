package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Cheese;
import br.com.lucenelanches.repository.CheeseRepository;
import br.com.lucenelanches.resource.CheeseResource;
import br.com.lucenelanches.util.CheeseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "cheeseService")
@Component
public class CheeseService {

    @Autowired
    CheeseRepository cheeseRepository;

    public List<CheeseResource> getAll(){

        List<Cheese> breads = cheeseRepository.findAll();

        List<CheeseResource> breadResources = CheeseConverter.convert(breads);

        return breadResources;
    }
}
