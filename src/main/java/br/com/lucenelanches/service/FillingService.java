package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Filling;
import br.com.lucenelanches.repository.FillingRepository;
import br.com.lucenelanches.resource.FillingResource;
import br.com.lucenelanches.util.FillingConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "fillingService")
@Component
public class FillingService {

    @Autowired
    FillingRepository fillingRepository;

    public List<FillingResource> getAll(){

        List<Filling> fillings = fillingRepository.findAll();

        List<FillingResource> breadResources = FillingConverter.convert(fillings);

        return breadResources;
    }
}
