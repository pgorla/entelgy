package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Salad;
import br.com.lucenelanches.repository.SaladRepository;
import br.com.lucenelanches.resource.SaladResource;
import br.com.lucenelanches.util.SaladConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "saladService")
@Component
public class SaladService {

    @Autowired
    SaladRepository saladRepository;

    public List<SaladResource> getAll(){

        List<Salad> salads = saladRepository.findAll();

        List<SaladResource> saladResources = SaladConverter.convert(salads);

        return saladResources;
    }
}
