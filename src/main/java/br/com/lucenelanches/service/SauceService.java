package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Sauce;
import br.com.lucenelanches.repository.SauceRepository;
import br.com.lucenelanches.resource.SauceResource;
import br.com.lucenelanches.util.SauceConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "sauceService")
@Component
public class SauceService {

    @Autowired
    SauceRepository sauceRepository;

    public List<SauceResource> getAll(){
        List<Sauce> sauces = sauceRepository.findAll();

        List<SauceResource> saladResources = SauceConverter.convert(sauces);

        return saladResources;
    }
}
