package br.com.lucenelanches.util;

import br.com.lucenelanches.entity.Bread;
import br.com.lucenelanches.resource.BreadResource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class BreadConverter {

    public static List<BreadResource> convertList(List<Bread> breads){

        List<BreadResource> breadResources = new ArrayList<>();

        for (Bread bread : breads) {
            BreadResource breadResource = new BreadResource();

            BeanUtils.copyProperties(bread, breadResource);

            breadResources.add(breadResource);

        }

        return breadResources;

    }

    public static BreadResource convert(Bread bread){


            BreadResource breadResource = new BreadResource();

            BeanUtils.copyProperties(bread, breadResource);


        return breadResource;

    }

}
