package br.com.lucenelanches.util;

import br.com.lucenelanches.entity.Cheese;
import br.com.lucenelanches.resource.CheeseResource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class CheeseConverter {

    public static List<CheeseResource> convert(List<Cheese> cheeses){

        List<CheeseResource> cheeseResources = new ArrayList<>();

        for (Cheese cheese : cheeses) {
            CheeseResource cheeseResource = new CheeseResource();

            BeanUtils.copyProperties(cheese, cheeseResource);

            cheeseResources.add(cheeseResource);

        }

        return cheeseResources;

    }

    public static CheeseResource convert(Cheese cheese){


        CheeseResource cheeseResource = new CheeseResource();

        BeanUtils.copyProperties(cheese, cheeseResource);


        return cheeseResource;

    }

}
