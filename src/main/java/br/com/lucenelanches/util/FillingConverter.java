package br.com.lucenelanches.util;

import br.com.lucenelanches.entity.Filling;
import br.com.lucenelanches.resource.FillingResource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class FillingConverter {

    public static List<FillingResource> convert(List<Filling> fillings){

        List<FillingResource> fillingResources = new ArrayList<>();

        for (Filling filling : fillings) {
            FillingResource fillingResource = new FillingResource();

            BeanUtils.copyProperties(filling, fillingResource);

            fillingResources.add(fillingResource);

        }

        return fillingResources;

    }

    public static FillingResource convert(Filling filling){


        FillingResource fillingResource = new FillingResource();

        BeanUtils.copyProperties(filling, fillingResource);


        return fillingResource;

    }

}
