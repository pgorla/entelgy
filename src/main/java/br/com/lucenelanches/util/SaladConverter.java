package br.com.lucenelanches.util;

import br.com.lucenelanches.entity.Salad;
import br.com.lucenelanches.resource.SaladResource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class SaladConverter {

    public static List<SaladResource> convert(List<Salad> salads){

        List<SaladResource> saladResources = new ArrayList<>();

        for (Salad salad : salads) {
            SaladResource saladResource = new SaladResource();

            BeanUtils.copyProperties(salad, saladResource);

            saladResources.add(saladResource);

        }

        return saladResources;

    }

    public static SaladResource convert(Salad salad){

        SaladResource saladResource = new SaladResource();

        BeanUtils.copyProperties(salad, saladResource);


        return saladResource;

    }

}
