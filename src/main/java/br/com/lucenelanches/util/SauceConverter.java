package br.com.lucenelanches.util;

import br.com.lucenelanches.entity.Sauce;
import br.com.lucenelanches.resource.SauceResource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class SauceConverter {

    public static List<SauceResource> convert(List<Sauce> sauces){

        List<SauceResource> sauceResources = new ArrayList<>();

        for (Sauce sauce : sauces) {
            SauceResource sauceResource = new SauceResource();

            BeanUtils.copyProperties(sauce, sauceResource);

            sauceResources.add(sauceResource);

        }

        return sauceResources;

    }

    public static SauceResource convert(Sauce sauce){

        SauceResource sauceResource = new SauceResource();

        BeanUtils.copyProperties(sauce, sauceResource);


        return sauceResource;

    }

}
