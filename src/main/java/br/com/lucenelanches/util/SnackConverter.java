package br.com.lucenelanches.util;

import br.com.lucenelanches.entity.Snack;
import br.com.lucenelanches.resource.SnackResource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class SnackConverter {

    public static List<SnackResource> convert(List<Snack> snacks){

        List<SnackResource> snackResources = new ArrayList<>();

        for (Snack snack : snacks) {
            SnackResource snackResource = new SnackResource();

            BeanUtils.copyProperties(snack, snackResource);

            snackResource.setBread(BreadConverter.convert(snack.getBread()));
            snackResource.setFilling(FillingConverter.convert(snack.getFilling()));
            snackResource.setCheese(CheeseConverter.convert(snack.getCheese()));
            snackResource.setSalad(SaladConverter.convert(snack.getSalad()));
            snackResource.setSpice(SpiceConverter.convert(snack.getSpice()));
            snackResource.setSauce(SauceConverter.convert(snack.getSauce()));

            snackResources.add(snackResource);

        }

        return snackResources;

    }

}
