package br.com.lucenelanches.util;

import br.com.lucenelanches.entity.Spice;
import br.com.lucenelanches.resource.SpiceResource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class SpiceConverter {

    public static List<SpiceResource> convert(List<Spice> spices){

        List<SpiceResource> spiceResources = new ArrayList<>();

        for (Spice spice : spices) {
            SpiceResource spiceResource = new SpiceResource();

            BeanUtils.copyProperties(spice, spiceResource);

            spiceResources.add(spiceResource);

        }

        return spiceResources;

    }

    public static SpiceResource convert(Spice spice){

        SpiceResource spiceResource = new SpiceResource();

        BeanUtils.copyProperties(spice, spiceResource);


        return spiceResource;

    }

}
