import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Order from './order';

ReactDOM.render(
    <Order />,
    document.getElementById('root')
);