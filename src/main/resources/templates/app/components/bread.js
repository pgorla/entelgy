import React, {Component} from 'react';
import $ from 'jquery';

class Bread extends Component {

    constructor(props) {
        super(props);
        this.state = {breads: []};
    }

    componentDidMount() {
        $.ajax({
                url: "/cheese",
                dataType: 'json',
                success: function (data) {
                    this.setState({breads: data});
                }.bind(this)
            }
        );
    }

    render() {
        return <select id="bread" name="bread" onChange={this.props.onChange} value={this.props.value}>
            {
                this.state.breads.map(function (bread) {
                    return (
                        <option key={bread.id} id={bread.id}>{bread.cheese}</option>
                    );
                })
            }
        </select>
    }
}
;

export default Bread;
