import React, {Component} from 'react';
import $ from 'jquery';

class Cheese extends Component {

    constructor(props) {
        super(props);
        this.state = {cheeses: []};
    }

    componentDidMount() {
        $.ajax({
                url: "/cheese",
                dataType: 'json',
                success: function (data) {
                    this.setState({cheeses: data});
                }.bind(this)
            }
        );
    }

    componentWillUpdate(nextProps, nextState){
        this.props = nextProps;
    }

    render() {
        return <select id="cheese" name="cheese" onChange={this.props.onChange} value={this.props.value}>
            <option>selecione</option>
            {
                this.state.cheeses.map(function(cheese){
                    return (
                        <option key={cheese.id} id={cheese.id}>{cheese.cheese}</option>
                    );
                })
            }
        </select>
    }
}
;

export default Cheese;
