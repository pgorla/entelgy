import React, {Component} from 'react';
import $ from 'jquery';

class Filling extends Component {

    constructor(props) {
        super(props);
        this.state = {fillings: []};
    }

    componentDidMount() {
        $.ajax({
                url: "/cheese",
                dataType: 'json',
                success: function (data) {
                    this.setState({fillings: data});
                }.bind(this)
            }
        );
    }

    render() {
        return <select id="filling" name="filling" onChange={this.props.onChange} value={this.props.value}>
            {
                this.state.fillings.map(function (filling) {
                    return (
                        <option key={filling.id} id={filling.id}>{filling.cheese}</option>
                    );
                })
            }
        </select>
    }
}
;

export default Filling;
