import React, {Component} from 'react';
import Cheese from './components/cheese';
import Bread from './components/bread';
import Filling from './components/filling';
import Snack from './components/filling';

class Order extends Component {

    constructor() {
        super();
        this.state = {order: {cheese: '', bread: '', filling: ''}}
    }

    snackChange(event) {
        this.setState({cheese: event.target.value})
    }

    handleChange(field, event) {
        var order = this.state.order;

        order[field] = event.target.value;

        this.setState({order: order})
    }

    render() {
        return <div>
            <Snack onChange={this.snackChange.bind(this)}/>
            <Cheese value={this.state.order.cheese} onChange={this.handleChange.bind(this, 'cheese')}/>
            <Bread value={this.state.order.bread} onChange={this.handleChange.bind(this, 'bread')}/>
            <Filling value={this.state.order.filling} onChange={this.handleChange.bind(this, 'filling')}/>
        </div>
    }
}
;

export default Order;
